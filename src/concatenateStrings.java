/*************************************************************************************************
 *  Name:Flavio Palacios Tinoco                                                                  *
 *  Assignement: Programming Assignment 1(Part 2)                                                *
 *  Description:                                                                                 *
 *      Write a Java program that generates a new string by concatenating the reversed           *
 *      substrings of even indexes and odd indexes separately from a given string. For this      *
 *      code you may use the previously built method to reverse the string.                      *
 *                                                                                               *
 *          Example #1                                                                           *
 *          Input: abscacd                                                                       *
 *          Output: dasaccb                                                                      *
 *                                                                                               *
 *          Explanation:                                                                         *
 *                    Substring are: asad, bcc                                                   *
 *                    Reversed substrings are: dasa, ccb                                         *
 *                    Output: dasaccb                                                            *
 *************************************************************************************************/

//Here the appropriate library is loaded to accept keyboard input.
import java.util.Scanner;

/******************************
    CLASS: Reverses a String
 ******************************/
class Reverse {
    //- - - - - MEMBER VARIABLES: - - - - -
    //"str" is a member variable. The "private" access
    //modifier is used to enforce encapsulation practice.
    private String str = "";

    //- - - - - SETTERS & GETTERS: - - - - -
    //These are this class' setter and getter.
    //The "public" access modifier is used to
    //enforce encapsulation practice.
    public String getStr () {
        return  str;
    }
    public void setString (String strContent) {
        str = strContent;
    }

    //- - - - - METHOD: revStr() - - - - -
    //Returns a reversed string when the given
    //parameter is a given string.
    String revStr (String givenString) {
        //The "index" variable will be used to break out of the
        //while loop.
        int index = (givenString.length()-1);

        //This loop will descend the "givenString" indexes, and as it does,
        //the "Reverse" class member variable "str" will have every
        //character appended to its current empty string.
        while (index != -1) {
            if (index == (givenString.length()-1)) {
                setString(getStr() + givenString.substring(index));
            }
            else {
                setString(getStr() + givenString.substring(index, index+1));
            }
            index--;
        }

        //Finally, "str" member variable contents are returned.
        return getStr();
    }
}

/********************************
    MAIN: Concatenates Strings
 ********************************/
public class concatenateStrings {
    public static void main(String[ ] arg) {
        //"givenString" is capable of containing a string.
        String givenString;

        //The "input" variable will hold keyboard input.
        //The final line in this block features "scan"
        //passing its contents to the "givenString" variable.
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a String: ");
        givenString = input.nextLine();

        //These two string variables will be used to make the
        //two substrings fom the "givenString".
        //Also, "givenStrLength" will tell how many characters
        //are in "givenString".
        String oddIndexesSubStr = "";
        String evenIndexesSubStr = "";
        int givenStrLength = givenString.length();

        if (givenStrLength == 0) {
            System.out.print("No input was provided.\n");
        }
        else {
            //This loop separates the even indexes form the odd indexes. Due to the specifications
            //of ".substing()", lots of control statements were used.
            for (int index = 0; (index <= givenStrLength-1); index++)
            {
                if (index == givenStrLength-1) {
                    oddIndexesSubStr = oddIndexesSubStr + givenString.substring(index);
                }
                else {
                    oddIndexesSubStr = oddIndexesSubStr + givenString.substring(index, index+1);
                    index++;
                }
                if ((index == givenStrLength-1) & (givenStrLength%2 == 0)) {
                    evenIndexesSubStr = evenIndexesSubStr + givenString.substring(index);
                }
                else if ((index < givenStrLength-1) & (givenStrLength%2 != 0)) {
                    evenIndexesSubStr = evenIndexesSubStr + givenString.substring(index, index+1);
                }
                else if ((index < givenStrLength-1) & (givenStrLength%2 == 0)){
                    evenIndexesSubStr = evenIndexesSubStr + givenString.substring(index, index+1);
                }
            }
        }

        System.out.print("Even Indexes Substring: " + evenIndexesSubStr +"\n");
        System.out.print("Odd Indexes Substring: " + oddIndexesSubStr +"\n\n");
        System.out.print("...REVERSING ABOVE SUBSTRINGS\n\n");

        //Two "Reverse" class reference variables are created.
        Reverse odd = new Reverse();
        Reverse even = new Reverse();

        //The "revStr()" method is invoked for the "odd" instance
        //and the "even" instance.
        oddIndexesSubStr = odd.revStr(oddIndexesSubStr);
        evenIndexesSubStr = even.revStr(evenIndexesSubStr);

        System.out.print("Reversed Even Indexes Substring: " + evenIndexesSubStr +"\n");
        System.out.print("Reversed Odd Indexes Substring: " + oddIndexesSubStr +"\n");
        String resultingString = evenIndexesSubStr;
        resultingString = resultingString + oddIndexesSubStr.substring(0);
        System.out.print("Both Reversed Substrings Above Concatenated: " + resultingString +"\n");
    }
}

/*#################################################################################################
                                          EXAMPLE OUTPUT
                                       --------------------
(Example 1)
Enter a String: boyoyo
Even Indexes Substring: ooo
Odd Indexes Substring: byy

...REVERSING ABOVE SUBSTRINGS

Reversed Even Indexes Substring: ooo
Reversed Odd Indexes Substring: yyb
Both Reversed Substrings Above Concatenated: oooyyb

(Example 2)
Enter a String: JavaIsSOOFUN!
Even Indexes Substring: aasOFN
Odd Indexes Substring: JvISOU!

...REVERSING ABOVE SUBSTRINGS

Reversed Even Indexes Substring: NFOsaa
Reversed Odd Indexes Substring: !UOSIvJ
Both Reversed Substrings Above Concatenated: NFOsaa!UOSIvJ


(Example 3)
Enter a String:
No input was provided.
Even Indexes Substring:
Odd Indexes Substring:

...REVERSING ABOVE SUBSTRINGS

Reversed Even Indexes Substring:
Reversed Odd Indexes Substring:
Both Reversed Substrings Above Concatenated:
#################################################################################################*/
